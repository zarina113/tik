import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Scanner;

public class Huffman {
    //узел
    class Node implements Comparable<Node> {
        final int sum;
        String code;

        void buildCode(String code) {
            this.code = code;
        }

        public Node(int sum) {
            this.sum = sum;
        }

        //метод сравнения для определения наименьшей частоты
        @Override
        public int compareTo(Node o) {
            return Integer.compare(sum, o.sum);
        }
    }
    
    //узел, имеющий потомков
    class InternalNode extends Node {
        Node left; //левый потомок
        Node right; //правый потомок

        @Override
        void buildCode(String code) {
            super.buildCode(code);
            left.buildCode(code + "0");
            right.buildCode(code + "1");
        }

        public InternalNode(Node left, Node right) {
            super(left.sum + right.sum);
            this.left = left;
            this.right = right;
        }
    }

    //лист, содержащий символ
    class LeafNode extends Node {
        char symbol;

        @Override
        void buildCode(String code) {
            super.buildCode(code);
            //вывод кода для каждого символа
            System.out.println(symbol + ": " + code);
        }

        public LeafNode(char symbol, int count) {
            super(count);
            this.symbol = symbol;
        }
    }

    private void run() throws FileNotFoundException {
        //чтение строки для кодирования
        Scanner input = new Scanner(new File("input.txt"));
        String s = input.next();

        //считаем частоту символов
        Map<Character, Integer> count = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i); //получение символа на i-ой позиции
            if (count.containsKey(c)) {
                //если такой символ уже есть в мн-ве, то увеличиваем частоту
                count.put(c, count.get(c) + 1);
            } else {
                count.put(c, 1);
            }
        }

        Map<Character, Node> charNodes = new HashMap<>();
        //запись в приоритетную очередь по частотам
        PriorityQueue<Node> priorityQueue = new PriorityQueue<>();
        for (Map.Entry<Character, Integer> entry : count.entrySet()) {
            LeafNode node = new LeafNode(entry.getKey(), entry.getValue());
            charNodes.put(entry.getKey(), node);
            priorityQueue.add(node);
        }

        //объединение символов наименьших частот в один узел (построение дерева)
        while (priorityQueue.size() > 1) {
            //извлекаем 1ый и 2ой узел с наименьшими частотами
            Node first = priorityQueue.poll();
            Node second = priorityQueue.poll();
            //объединяем в один узел
            InternalNode node = new InternalNode(first, second);
            //кладем новый узел в очередь
            priorityQueue.add(node);
        }

        //построение кода для каждого символа
        Node root = priorityQueue.poll();
        if (count.size()==1) {
            root.code = "0";
        } else {
            root.buildCode("");
        }

        //получение закодированной строки
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            result += charNodes.get(c).code;
        }
        System.out.println(result);

    }

    public static void main(String[] args) throws FileNotFoundException {
        new Huffman().run();
    }
}
